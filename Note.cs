using System;
using System.Collections.Generic;
using System.Text;

namespace LV1
{
    class Note
    {
        private string content;
        private string author;
        private int importance;

        public Note()
        {
            this.content = "Empty";
            this.author = "No author";
            this.importance = 0;
        }

        public Note(string author)
        {
            this.content = "No content";
            this.author = author;
            this.importance = 0;
        }

        public Note(string content, string author, int importance)
        {
            this.content = content;
            this.author = author;
            this.importance = importance;
        }

        public string Content
        {
            get { return this.content; }
            set { this.content = value; }
        }

        public int Importance
        {
            get { return this.importance; }
            set { this.importance = value; }
        }

        public override string ToString()
        {
            return "Content: " + this.content + "\n" + "Author: " + this.author;
        }

        public string getContent() { return this.content; }
        public string getAuthor() { return this.author; }
        public int getImportance() { return this.importance; }

        public void setContent(string content) { this.content = content; }

        public void setImportance(int importance) { this.importance = importance; }
    }
}