using System;
using System.Collections.Generic;
using System.Text;

namespace LV1
{
    class NoteTime : Note
    {
        private DateTime time;

        public NoteTime() : base()
        {
            time = DateTime.Now;
        }

        public NoteTime(string content, string author, int importance) : base(content, author, importance)
        {
            this.time = DateTime.Now;
        }

        public NoteTime(string content, string author, int importance, DateTime time) : base(content, author, importance)
        {
            this.time = time;
        }

        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }

        public override string ToString()
        {
            return base.ToString() + "\n" + this.Time;
        }
    }
}
