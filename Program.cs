using System;

namespace LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note1 = new Note();
            Console.WriteLine("Content: " + note1.getContent());
            Console.WriteLine("Author: " + note1.getAuthor() + "\n");

            Note note2 = new Note("Author1");
            Console.WriteLine("Content: " + note2.getContent());
            note2.setContent("New Content");
            Console.WriteLine("New Content: " + note2.getContent());
            Console.WriteLine("Author: " + note2.getAuthor() + "\n");

            Note note3 = new Note("Content2", "Author2", 5);
            Console.WriteLine("Content: " + note3.getContent());
            Console.WriteLine("Author: " + note3.getAuthor());
            Console.WriteLine("Importance: " + note3.Importance);
            Console.WriteLine("ToString() -> " + note3.ToString() + "\n");

            Note note4 = new NoteTime("Content3", "Author3", 3);
            Console.WriteLine(note4.ToString());
        }
    }
}
